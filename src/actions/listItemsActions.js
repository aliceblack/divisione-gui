import dispatcher from "../dispatcher";

export function addItem(text){
	dispatcher.dispatch({
	type: "ADD_ITEM",
	text,
      });
}
