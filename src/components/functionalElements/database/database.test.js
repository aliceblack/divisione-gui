import { fromJS } from 'immutable';
import { Database } from './database';

describe('Class Database adapter', () => {
    it('should have the correct mongo url field', () => {
        const database = new Database('mongodb://localhost:27017/test');
        expect(database.mongourl).toEqual('mongodb://localhost:27017/test');
    });
});
