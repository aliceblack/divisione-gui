const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

//design pattern adapter 
export class Database {
    constructor(mongourl){
        this.mongourl = mongourl;
    }

    insertOne(collectionName, element){
        MongoClient.connect(this.mongourl, function(err, db) {
            assert.equal(null, err);
            console.log("Connected correctly to server");
            db.collection(collectionName).insertOne({element}, function(err, r) {
                assert.equal(null, err);
                assert.equal(1, r.insertedCount);
                db.close();
            });
        });
    }
    insertMany(collectionName, elements){
        MongoClient.connect(this.mongourl, function(err, db) {
            assert.equal(null, err);
            console.log("Connected correctly to server");
            db.collection(collectionName).insertMany({elements}, function(err, r) {
                assert.equal(null, err);
                assert.equal(1, r.insertedCount);
                db.close();
            });
        });
    }


}



/*
esempio di uso
adapter = new Database('mongodb://localhost:27017/test');
adapter.connect().then(
    (db)=>{db.close(); console.log("connection enstablished");
}).catch((err)=>{
    console.log(err);
});
*/