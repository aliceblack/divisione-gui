import { EventEmitter } from "events";
import dispatcher from "../../../dispatcher.js";


class ItemsStore extends EventEmitter{
	constructor(){
		super()
		this.items=[
		 /*{
		    item: 'mela',
		 },
		 {
		    item: 'pera avvelenata',
		 },*/
		];
	}

	addItems(text){
		this.items.push({item: text,});
		this.emit("change");
	}

	getItems(){
		return this.items;
	}

	handleActions(action){
	  switch(action.type){
	     case "ADD_ITEM": {
		   this.addItems(action.text);
		   break;
		
	    }
	    default: break;
	  }
	}

}

const itemsStore= new ItemsStore();
dispatcher.register(itemsStore.handleActions.bind(itemsStore));
window.dispatcher=dispatcher;

export default itemsStore;
