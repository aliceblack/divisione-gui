import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

export default class RadioButton extends Component {
    render() {
        const { onChange, value, groupName, checked } = this.props;
        const optional = {};
        if (typeof onChange === 'function') {
            optional.onChange = onChange.bind(this.props.group, this.props.value);
        }
        if (checked === true) {
            optional.defaultChecked = {};
        }

        return (
            <div>
                <input
                  type="radio"
                  value={value}
                  name={groupName}
                  id={`${value}radio`}
                  {...optional}
                />
                <label htmlFor={`${value}radio`}>{this.props.label}</label>
            </div>
        );
    }
}

RadioButton.propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.image,
    ]).isRequired,
    groupName: PropTypes.string.isRequired,
    checked: PropTypes.bool,
    label: PropTypes.string.isRequired,
};

RadioButton.defaultProps = {
    checked: false,
};
