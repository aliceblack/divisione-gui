import React, { Component } from 'react';
import RadioButton from './RadioButton';

export default class RadioButtonsGroup extends Component {
    constructor() {
        super();
        this.state = {
            selected: null,
        };
    }

    onCheckedChange(value) {
        this.setState({ selected: value });
        console.log(value);
    }

    processInput() {
        return this.props.buttons.map(
            ({ value, label, checked }, i) =>
                <RadioButton
                  group={this} key={i} value={value}
                  label={label} groupName={this.props.groupName}
                  checked={checked} onChange={this.onCheckedChange}
                />,
        );
    }

    render() {
        const buttons = this.processInput();
        return (
            <div>
                {buttons}
            </div>
        );
    }
}
