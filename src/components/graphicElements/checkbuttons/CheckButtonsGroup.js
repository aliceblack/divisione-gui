import React, { Component } from 'react';
import CheckButton from './CheckButton';

export default class CheckButtonsGroup extends Component {
    // onCheckedChange(object) {
    //     console.log(object);
    // }

    processInput() {
        return this.props.buttons.map(
            ({ name, value, label, checked }) =>
                <CheckButton
                  key={name} name={name} value={value} checked={checked}
                  label={label}
                />,
        );
    }

    render() {
        const buttons = this.processInput();
        return (
            <div>
                {buttons}
            </div>
        );
    }
}
