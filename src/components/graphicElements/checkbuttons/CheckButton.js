import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

export default class CheckButton extends Component {
    constructor() {
        super();
        this.handleOnChange = this.handleOnChange.bind(this);
        this.state = {
            checked: false,
        };
    }
    handleOnChange() {
        this.setState({ checked: !this.state.checked });
        console.log(this.state.checked);
    }
    render() {
        return (
            <div>
                <input
                  type="checkbox" name={this.props.name} value={this.props.value}
                  checked={this.state.checked}
                  onChange={this.handleOnChange}
                  id={this.props.name}
                />
                <label htmlFor={this.props.name}>{this.props.label}</label>
            </div>
        );
    }
}

CheckButton.propTypes = {
    name: PropTypes.string.isRequired,
    value: PropTypes.string,
    checked: PropTypes.bool,
    label: PropTypes.string,
};

CheckButton.defaultProps = {
    value: 'Default value',
    label: 'Default value',
    checked: false,
};
