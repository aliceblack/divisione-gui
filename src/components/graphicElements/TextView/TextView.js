import React from 'react';
import PropTypes from 'prop-types';

export default function TextView({ text }) {
    return <p>{text}</p>;
}

TextView.propTypes = {
    text: PropTypes.string,
};

TextView.defaultProps = {
    text: '',
};
