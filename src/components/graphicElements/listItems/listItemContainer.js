import React, { Component } from 'react';
import ListItem from './listItem';
import InputText from './../inputText/inputText';
import Button from './../button/button';
import ItemsStore from './../../functionalElements/itemsStore/itemsStore';
import * as ListItemsActions from './../../../actions/listItemsActions'; //objectliteral

export default class ListItemContainer extends Component {
    constructor(){
	super();
	this.state={
		items: ItemsStore.getItems(),
	};
    }

    processInput() {
        return this.state.items.map(
            ({ item }) =>
                <ListItem
                  item={item}
                />,
        );
    }

    componentWillMount(){
	ItemsStore.on("change", ()=>{
	  this.setState({
	    items: ItemsStore.getItems(),
	  });
	});
    }

    handleSubmit(event) {
	var itemString = document.getElementById('todoInpuId').value;
	event.preventDefault();//prevent page refresh
	ListItemsActions.addItem(itemString);
    }

    render() {
        const items = this.processInput();
        return (
            <div>
		<form name="submitItemForm" onSubmit={this.handleSubmit}>
			<InputText theid="todoInpuId"/><input type="submit" name="itemText"/>
		</form>
                {items}
		<button>Rimuovi selezionati</button>
            </div>
        );
    }
}
