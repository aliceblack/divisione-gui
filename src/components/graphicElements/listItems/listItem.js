import React from 'react';
import CheckButtonsGroup from '../checkbuttons/CheckButtonsGroup';

export default class ListItem extends React.Component {

constructor(props) { //passing props
        super(props);
        this.state = {
            //theid: this.props.theid,
	    item: this.props.item,
        };
}

    render(){
        return(
		<div>
			<CheckButtonsGroup
                  groupName="lista" buttons={[
                      {
                          name: this.props.item,
                          value: this.props.item,
                          label: this.props.item,
                          checked: false,
                      },
                  ]}
                />
		</div>
            );
    }
}
