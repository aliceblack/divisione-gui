import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom';

import Counter from './components/index';
import counter from './reducers/reducer';
import GenericBubble from './components/genericBubble/genericBubble';

const store = createStore(counter);
const rootEl = document.getElementById('root');

class HomeRoute extends React.Component {
    render() {
        return (
            <Counter
              value={(store.getState()).counter}
              onIncrement={() => store.dispatch({ type: 'INCREMENT' })}
              onDecrement={() => store.dispatch({ type: 'DECREMENT' })}
            />
        );
    }
}

const render = () => ReactDOM.render(
    <Router>
        <div>
            <Route exact path="/" component={HomeRoute} />
            <Route path="/bubble/" component={GenericBubble} />
        </div>
    </Router>,
  rootEl,
);

render();
store.subscribe(render);
